Feature: Search movie reviews

  Scenario: A list of matching movies is returned for a request made with an existing movie title
    Given the request with a valid movie title "The Godfather"
    When the request is made with the get method to search for the movie
    Then a response with http status equal to 200 is returned
    And the response body matches "reviewsSchema.json"
    
  Scenario: A 401 code and error message is returned for a search request made without mandatory api key
    Given the request with a valid movie title "The Godfather" but no api key
    When the request is made with the get method to search for the movie
    Then a response with http status equal to 401 is returned
    And the response body matches "errorSchema.json"