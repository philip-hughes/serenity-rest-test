package endpoints;

public enum MovieReviewsEndpoints {
    SEARCH("reviews/search.json?query={query}&api-key={key}");
    //PICKS("reviews/picks.json?api-key={key}");
	
	
	private final String url;
	
	MovieReviewsEndpoints(String url){
		this.url = url;
	}
	
	public String getUrl() {
		return url;
	}
	
}
