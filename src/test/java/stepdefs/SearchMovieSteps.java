package stepdefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import endpoints.MovieReviewsEndpoints;
import net.serenitybdd.rest.SerenityRest;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class SearchMovieSteps extends BaseTest {
	
	@Given("the request with a valid movie title {string}")
	public void the_request_with_a_valid_movie_title(String movie) {
		SerenityRest.given().baseUri(MOVIES_BASE_URI)
		  .pathParam("query", movie)
		  .pathParam("key", API_KEY);
	}

	@Given("the request with a valid movie title {string} but no api key")
	public void the_request_with_a_valid_movie_title_but_no_api_key(String movie) {
		SerenityRest.given().baseUri(MOVIES_BASE_URI)
		  .pathParam("query", movie)
		  .pathParam("key", "");
	}
	

	@When("the request is made with the get method to search for the movie")
	public void the_request_is_made_with_the_get_method_to_search_for_the_movie() {
		SerenityRest.when().get(MovieReviewsEndpoints.SEARCH.getUrl());
	}
	   
    
    @Then("a response with http status equal to {int} is returned")
    public void a_response_with_http_status_equal_to_is_returned(Integer statusCode) {
		SerenityRest.lastResponse().then().assertThat().statusCode(statusCode);
    }


    @Then("the response body matches {string}")
    public void the_response_body_matches(String schema) {  	
    	SerenityRest.lastResponse().then().assertThat().body(matchesJsonSchemaInClasspath("schemas/" + schema)); 
    }

}
