# Serenity Rest Test

This is a java based Serenity and Cucumber project for the purpose of demonstrating the testing of a REST 
API within a BDD framework.  

## Endpoints
The API chosen for the project is the New York times movie review API
https://developer.nytimes.com/docs/movie-reviews-api/1/overview

The Reviews API exposes three paths
 - GET /critics/{reviewer}.json
 - GET /reviews/{type}.json
 - GET /reviews/search.json

For this project the reviews/search.json path is tested.  Search requests require a search query and
mandatory API key as parameters.  The tests in this project verify that:
 1. Valid search queries return a list of 1 or more matching movies
 2. That the backend gracefully handles the omission of the mandatory API key from the request.

Responses for each test are verified against the json schemas defined in the schemas directory:

```	
	+ src/test/resources
		features
		schemas
		- errorSchema.json
		- reviewsSchema.json
```

These schemas were manually created to verify that all required fields are present in the response body, and that
the type for each field is correct.  For some fields an expected value was also defined in the 
schemas using the enum property. 
 
Tests will fail if:
1. Any required fields are missing from the respective response body.
2. Any field type in the response is not as expected.
3. Any fields types are null or empty. Note however that for some fields such as 'multimedia', null is specified in the schema as a valid type, and
	so will not result in a failed test.
4. Any field values don't match those specified in the schema.  Note that only a few fields have an expected value.  	

```
	@Given("the request with a valid movie title {string}")
	public void the_request_with_a_valid_movie_title(String movie) {
		SerenityRest.given().baseUri(MOVIES_BASE_URI)
		  .pathParam("query", movie)
		  .pathParam("key", API_KEY);
	}

	@Given("the request with a valid movie title {string} but no api key")
	public void the_request_with_a_valid_movie_title_but_no_api_key(String movie) {
		SerenityRest.given().baseUri(MOVIES_BASE_URI)
		  .pathParam("query", movie)
		  .pathParam("key", "");
	}
	
	@When("the request is made with the get method to search for the movie")
	public void the_request_is_made_with_the_get_method_to_search_for_the_movie() {
		SerenityRest.when().get(MovieReviewsEndpoints.SEARCH.getUrl());
	}
	  
    @Then("a response with http status equal to {int} is returned")
    public void a_response_with_http_status_equal_to_is_returned(Integer statusCode) {
		SerenityRest.lastResponse().then().assertThat().statusCode(statusCode);
    }
    
    @Then("the response body matches {string}")
    public void the_response_body_matches(String schema) {  	
    	SerenityRest.lastResponse().then().assertThat().body(matchesJsonSchemaInClasspath("schemas/" + schema)); 
    }
```
	

## Installation and running tests
Prerequisites:
- Maven
- Java 8

To get the code open a command window and run:

    git clone https://gitlab.com/philip-hughes/serenity-rest-test.git
    cd serenity-rest-test/

To run the tests:

    mvn clean verify

## Adding new tests

```
	+ src/test/java
		cukes
		endpoints
		stepdefs
	+ src/test/resources
		features
		schemas
```

1. Create a new feature file in the features package e.g. NewTest.feature
2. Define the Feature name and Scenario using the Gherkin syntax.
3. Run the project and check the console output for  the step snippets you can use in the step definitions e.g.
	You can implement missing steps with the snippets below:

	@Given("my test prequisites")
	public void my_test_prequisites() {
    	// Write code here that turns the phrase above into concrete actions
    	throw new cucumber.api.PendingException();
	}
4. Go to the stepdefs package and create a new class e.g. MyNewTestSteps.java.
5. Open the class file and extend the BaseTest class.
6. Paste the code snippets mentioned above into your class file and write your code for each step.

## Reports
For each build in the Gitlab CI environment a report artifact is created.  Reports can be viewed using
the following link and replacing the job number as required:
 - https://philip-hughes.gitlab.io/-/serenity-rest-test/-/jobs/1251381372/artifacts/target/site/serenity/index.html

